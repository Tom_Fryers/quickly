# Quickly

Quickly is a fast input method for Lilypond. It is intended for quickly getting
notes in Lilypond format, to paste into a document. Quickly is not designed for
complicated editing of already-written notes.

## Dependencies

- Python 3.9+
- Pygame

## Usage

Quickly is Pip-installable, but can also just be run with `./app`. After opening
it, you should see a staff.

- Press T, then click options to seTup the score.
- Left-click the staff to add a note. Middle click to add a rest. This can also be
  used to turn an earlier note into a rest..
- Press Z to undo the last action.
- Press B (or Backspace) to delete the last note/chord.
- Press C to Copy the Lilypond code to the clipboard.

These controls edit a note before it is added to the score:

- Scroll to change the note duration.
- F and S control Flats, Sharps and naturals.
- D (or .) adds Dots.
- A Attaches a tie or slur from the previous note.
- E adds Extra stuff.

Clicking near previously added notes will form chords. To replace them instead,
middle click to create a rest, then place the new note on top.

## Credits

The fonts are [Leland](https://github.com/MuseScoreFonts/Leland), for some of
the symbols and the icon, and [Edwin](https://github.com/MuseScoreFonts/Edwin)
for the text. Both are licenced under the SIL Open Font License. See the
`fonts/` directory for details.

---

Quickly Copyright © 2020–2022 Tom Fryers

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
