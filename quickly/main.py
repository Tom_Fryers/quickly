from __future__ import annotations

import argparse
import copy
from contextlib import suppress
from dataclasses import dataclass

import pygame

from . import music, render

WIDTH = 1280
HEIGHT = 720
DIMS = (WIDTH, HEIGHT)

BLACK = (0, 0, 0)

MINIPIECE_HEIGHT = 0.31


class ProgramExit(Exception):
    pass


def restrict(value, minimum, maximum):
    return max(min(value, maximum), minimum)


def menu(S, options, font):
    selected = None
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                raise ProgramExit
            elif event.type == pygame.MOUSEBUTTONDOWN and selected is not None:
                return selected
        S.fill((255, 255, 255))
        position = [1, 1]
        for option, image in options.items():
            mp = pygame.mouse.get_pos()
            if all(
                position[i] - 0.5 < mp[i] / (DIMS[i] / 5) < position[i] + 0.5
                for i in range(2)
            ):
                selected = option
                textimage = font.render(option, 1, BLACK, (100, 100, 100))
            else:
                textimage = font.render(option, 1, BLACK)
            drawpos = (
                round(position[0] * WIDTH / 5 - image.get_width() / 2),
                round(position[1] * HEIGHT / 5 - image.get_height() / 2),
            )
            textdrawpos = (
                round(position[0] * WIDTH / 5 - textimage.get_width() / 2),
                round(
                    position[1] * HEIGHT / 5
                    - image.get_height() / 2
                    + textimage.get_height()
                ),
            )
            S.blit(image, drawpos)
            S.blit(textimage, textdrawpos)
            position[0] += 1
            if position[0] >= 5:
                position[0] = 1
                position[1] += 1
        pygame.display.update()


@dataclass
class Option:
    name: str
    key: int
    suboptions: tuple[Option, ...] | None = None

    @property
    def menu_text(self):
        text = f"{pygame.key.name(self.key)}: {self.name}"
        if self.suboptions is None:
            return text
        return text + "..."

    def __contains__(self, key):
        return any(key == s.key for s in self.suboptions)

    def __getitem__(self, key):
        for sub in self.suboptions:
            if key == sub.key:
                return sub
        raise KeyError("invalid key for this option")

    def __iter__(self):
        return iter(self.suboptions)


ARTICULATIONS = Option(
    "extras",
    pygame.K_e,
    (
        Option(
            "articulations",
            pygame.K_a,
            (
                Option("accent", pygame.K_a),
                Option("espressivo", pygame.K_e),
                Option("marcato", pygame.K_r),
                Option("portato", pygame.K_d),
                Option("staccatissimo", pygame.K_c),
                Option("staccato", pygame.K_s),
                Option("tenuto", pygame.K_t),
            ),
        ),
        Option(
            "ornament",
            pygame.K_r,
            (
                Option("mordent", pygame.K_d),
                Option("trill", pygame.K_e),
                Option("reverse turn", pygame.K_r),
                Option("turn", pygame.K_t),
            ),
        ),
        Option(
            "fermata",
            pygame.K_f,
            (
                Option("very short fermata", pygame.K_a),
                Option("short fermata", pygame.K_s),
                Option("fermata", pygame.K_f),
                Option("long fermata", pygame.K_e),
                Option("very long fermata", pygame.K_v),
            ),
        ),
        Option(
            "dynamics",
            pygame.K_d,
            (
                Option("ppppp", pygame.K_r),
                Option("pppp", pygame.K_e),
                Option("ppp", pygame.K_w),
                Option("pp", pygame.K_q),
                Option("p", pygame.K_a),
                Option("mp", pygame.K_s),
                Option("mf", pygame.K_d),
                Option("f", pygame.K_f),
                Option("ff", pygame.K_v),
                Option("fff", pygame.K_c),
                Option("ffff", pygame.K_x),
                Option("fffff", pygame.K_z),
            ),
        ),
    ),
)

MINI_MENU_LINE_HEIGHT = 35


def mini_menu(S, options_tree, font):
    background = S.copy()
    option_images = [
        font.render(option.menu_text, True, BLACK) for option in options_tree
    ]
    while True:
        # Clear the screen early so that it can be passed to children correctly
        S.blit(background, (0, 0))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                raise ProgramExit
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    return None
                if event.key in options_tree:
                    if options_tree[event.key].suboptions is None:
                        return options_tree[event.key].name
                    return mini_menu(S, options_tree[event.key], font)
        for i, option in enumerate(option_images):
            S.blit(
                option,
                (
                    (i // 5) * HEIGHT / 3 + HEIGHT * 0.03,
                    (i % 5) * font.get_linesize() + HEIGHT * 0.03,
                ),
            )
        pygame.display.update()


@dataclass(frozen=True)
class ExtraNote:
    pos: int | str
    note: music.Note
    joined: bool
    articulations: set[str]


def run():
    global WIDTH, HEIGHT, DIMS
    pygame.init()

    S = pygame.display.set_mode(DIMS, pygame.RESIZABLE)
    pygame.display.set_caption("Quickly")
    pygame.display.set_icon(pygame.image.load("graphics/icon.png"))

    pygame.scrap.init()

    clock = pygame.time.Clock()

    piece = render.Piece(
        music.NORMALCLEFS["treble"],
        music.NORMALKEYS["C major/A minor"],
        music.TimeSignature(4, 4),
        [],
        HEIGHT * AA,
    )
    currentnote = 5
    currentdots = 0
    currentsharpness = 0
    currentjoined = False
    extranote = None
    last_extranote = None
    articulations = set()

    undostate = None

    MENUFONT = pygame.font.Font("fonts/Edwin/Edwin-Roman.otf", round(HEIGHT * 0.04))

    noteloc = None

    changed = True

    ext = False
    while not ext:
        mp = pygame.mouse.get_pos()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                raise ProgramExit
            elif event.type == pygame.VIDEORESIZE:
                DIMS = S.get_size()
                WIDTH, HEIGHT = DIMS
                piece.set_lh(HEIGHT * AA)
                MENUFONT = pygame.font.Font(
                    "fonts/Edwin/Edwin-Roman.otf", round(HEIGHT * 0.04)
                )
                changed = True
            elif event.type == pygame.MOUSEBUTTONDOWN:
                changed = True
                # Scrolling to change note length
                if event.button == 5:
                    currentnote -= 1
                elif event.button == 4:
                    currentnote += 1
                currentnote = restrict(currentnote, 0, 11)

                # New note
                if event.button == 1 and extranote:
                    currentdots = 0
                    undostate = copy.deepcopy(piece.notes)
                    currentsharpness = 0
                    currentjoined = False
                    articulations = set()
                    if noteloc == "new":
                        piece.append_note(
                            extranote.note,
                            joined=extranote.joined,
                            articulations=extranote.articulations,
                        )
                    # Chord
                    else:
                        index = piece.notes.index(extranote.pos)
                        # Ensure note is correct length and not already present
                        chord = piece.notes[index]
                        if piece.notes[index].time == extranote.note.time and not any(
                            extranote.note.pitch == note.pitch for note in chord
                        ):
                            newnotes = [*chord.notes, extranote.note]
                            piece.notes[index] = music.Chord(
                                newnotes,
                                joined=extranote.joined,
                                articulations=extranote.articulations,
                            )

                # New rest
                if event.button == 2:
                    currentdots = 0
                    undostate = copy.deepcopy(piece.notes)
                    if noteloc == "new":
                        piece.append_note(music.Rest(currentnote, currentdots))
                    else:
                        # Delete old notes
                        index = piece.notes.index(extranote.pos)
                        note = piece.notes[index]
                        piece.notes[index] = music.Rest(note.duration, note.dots)

            elif event.type == pygame.KEYDOWN:
                changed = True
                # Copy
                if event.key == pygame.K_c:
                    pygame.scrap.put(
                        "text/plain;charset=utf-8", piece.to_lilypond().encode()
                    )
                # Delete end note or chord
                elif event.key in (pygame.K_b, pygame.K_BACKSPACE):
                    piece.notes = piece.notes[:-1]
                # Add dots
                elif event.key in (pygame.K_d, pygame.K_PERIOD):
                    currentdots += 1
                    if currentdots > 3:
                        currentdots = 0
                # Sharps and flats
                elif event.key == pygame.K_s:
                    currentsharpness += 1
                elif event.key == pygame.K_f:
                    currentsharpness -= 1
                # Undo
                elif event.key == pygame.K_z:
                    if undostate is not None:
                        piece.notes, undostate = undostate, piece.notes
                elif event.key == pygame.K_v:
                    pass  # Import
                elif event.key == pygame.K_t:
                    clefims = {}
                    for clefname, clef in music.NORMALCLEFS.items():
                        fakepiece = render.Piece(
                            clef,
                            piece.key,
                            piece.time_signature,
                            [],
                            HEIGHT * MINIPIECE_HEIGHT * AA,
                        )
                        clefims[clefname] = fakepiece.draw(
                            (WIDTH / 5, HEIGHT / 5), None, AA
                        )
                    piece = render.Piece(
                        music.NORMALCLEFS[menu(S, clefims, MENUFONT)],
                        piece.key,
                        piece.time_signature,
                        piece.notes,
                        HEIGHT * AA,
                    )
                    keyims = {}
                    for keyname, key in music.NORMALKEYS.items():
                        fakepiece = render.Piece(
                            piece.clef,
                            key,
                            piece.time_signature,
                            [],
                            HEIGHT * MINIPIECE_HEIGHT * AA,
                        )
                        keyims[keyname] = fakepiece.draw(
                            (WIDTH / 5, HEIGHT / 5), None, AA
                        )
                    piece = render.Piece(
                        piece.clef,
                        music.NORMALKEYS[menu(S, keyims, MENUFONT)],
                        piece.time_signature,
                        piece.notes,
                        HEIGHT * AA,
                    )
                    timeims = {}
                    for timename, time in music.COMMONTIMESIGNATUES.items():
                        fakepiece = render.Piece(
                            piece.clef,
                            piece.key,
                            time,
                            [],
                            HEIGHT * MINIPIECE_HEIGHT * AA,
                        )
                        timeims[timename] = fakepiece.draw(
                            (WIDTH / 5, HEIGHT / 5), None, AA
                        )
                    piece = render.Piece(
                        piece.clef,
                        piece.key,
                        music.COMMONTIMESIGNATUES[menu(S, timeims, MENUFONT)],
                        piece.notes,
                        HEIGHT * AA,
                    )
                elif event.key == pygame.K_a:
                    currentjoined = not currentjoined
                elif event.key == pygame.K_e:
                    new_articulation = mini_menu(S, ARTICULATIONS, MENUFONT)
                    if new_articulation is None:
                        break
                    if new_articulation in articulations:
                        articulations.remove(new_articulation)
                    else:
                        articulations.add(new_articulation)

        currentsharpness = restrict(currentsharpness, -3, 3)

        # Determine position of new note
        for note_area in piece.note_areas_x:
            if note_area[0] <= mp[0] <= note_area[1]:
                # Check same note isn't already in the piece
                if piece.note_areas_x[note_area] != "same":
                    noteloc = piece.note_areas_x[note_area]
                break
        else:
            noteloc = "new"

        # Determine pitch of new note
        notepitch = None
        for note_area in piece.note_areas_y:
            if note_area[0] <= mp[1] <= note_area[1]:
                sharpness = (
                    currentsharpness
                    + piece.key.notesharps[piece.note_areas_y[note_area].note]
                )
                sharpness = restrict(sharpness, -2, 2)
                notepitch = piece.note_areas_y[note_area].sharpened(sharpness)

        if notepitch is not None:
            extranote = ExtraNote(
                noteloc,
                music.Note(notepitch, currentnote, dots=currentdots),
                currentjoined,
                articulations,
            )
        else:
            extranote = None

        if extranote != last_extranote or changed:
            S.blit(piece.draw(DIMS, extranote, aa=AA), (0, 0))
            changed = False
            last_extranote = extranote
            pygame.display.update()
        clock.tick(120)


def main():
    global AA
    parser = argparse.ArgumentParser()
    parser.add_argument("--anti-aliasing", type=int, default=1)
    args = parser.parse_args()
    AA = args.anti_aliasing
    with suppress(ProgramExit):
        run()
