import math
from contextlib import suppress
from dataclasses import dataclass, field

NOTENAMES = (
    "maxima",
    "longa",
    "breve",
    "semibreve",
    "minim",
    "crotchet",
    "quaver",
    "semiquaver",
    "demisemiquaver",
    "hemidemisemiquaver",
    "semihemidemisemiquaver",
    "demisemihemidemisemiquaver",
)
ARTICULATION_SHORT_NAMES = {
    "accent": ">",
    "marcato": "^",
    "portato": "_",
    "staccatissimo": "!",
    "staccato": ".",
    "tenuto": "-",
}
PITCHES = "ABCDEFG"


def note_of_length(length):
    """Get the required note and dots for a given length."""
    note = math.ceil(5 - math.log(length, 2))
    extendby = length / 2 ** (5 - note)
    dots = int(-math.log(2 - extendby, 2))
    return note, dots


@dataclass
class Pitch:
    """A pitch with an octave."""

    note: str
    octave: int
    sharpness: int = 0

    @classmethod
    def from_int(cls, number):
        """Convert an integer to a Pitch instance."""
        return cls(PITCHES[number % 7], number // 7)

    def sharpened(self, amount):
        return Pitch(self.note, self.octave, sharpness=self.sharpness + amount)

    def sharpnesstext(self):
        if self.sharpness > 0:
            return "s" * self.sharpness
        if self.sharpness < 0:
            return "f" * -self.sharpness
        return ""

    def __int__(self):
        return self.octave * 7 + PITCHES.index(self.note)


CLEFS = {"G": Pitch("G", 4), "C": Pitch("C", 4), "F": Pitch("F", 3)}


@dataclass
class Note:
    """A musical note, with pitch and duration."""

    pitch: int
    duration: int
    dots: int = 0

    def __post_init__(self):
        self.time = 2 ** (5 - self.duration) * (2 - 2**-self.dots)


@dataclass
class Chord:
    """A collection of notes."""

    notes: list[Note]
    joined: bool = False
    articulations: set[str] = field(default_factory=set)

    def __post_init__(self):
        self.duration = self.notes[0].duration
        self.dots = self.notes[0].dots
        self.time = self.notes[0].time

    def same_pitches(self, other):
        return {int(x.pitch) for x in self} == {int(x.pitch) for x in other}

    def __hash__(self):
        return id(self)

    def __iter__(self):
        yield from self.notes

    def __getitem__(self, index):
        return self.notes[index]

    def __len__(self):
        return len(self.notes)


class Rest(Chord):
    """A special zero-size chord."""

    def __init__(self, duration, dots):
        fakenote = Note(Pitch("A", 1), duration, dots=dots)
        super().__init__([fakenote])
        self.notes = []

    def __repr__(self):
        return f"Rest({self.duration!r}, {self.dots!r})"


@dataclass
class Clef:
    """Defines a postion of a note on the staff."""

    note: str
    position: int

    def __post_init__(self):
        self.bottomline = int(CLEFS[self.note]) - self.position


@dataclass
class Key:
    """A collection of sharps and flats."""

    notesharps: dict[str, int]

    def __post_init__(self):
        for note in "ABCDEFG":
            if note not in self.notesharps:
                self.notesharps[note] = 0

    @classmethod
    def from_sharps(cls, notes):
        """Return a Key containing the given sharps."""
        return cls({x: 1 for x in notes})

    @classmethod
    def from_flats(cls, notes):
        """Return a Key containing the given flats."""
        return cls({x: -1 for x in notes})


@dataclass
class TimeSignature:
    """A time signature with an upper and lower value."""

    upper: int
    lower: int

    def __post_init__(self):
        self.duration = 4 * self.upper / self.lower

    def __getitem__(self, index):
        if index == 0:
            return self.upper
        if index == 1:
            return self.lower
        raise IndexError(f"Invalid index for TimeSignature: {index}")


@dataclass
class Piece:
    clef: Clef
    key: Key
    time_signature: TimeSignature
    notes: list[Chord]

    def append_note(self, note, joined=False, articulations=None):
        duration = note.time
        timeleft = (
            self.time_signature.duration - self.duration % self.time_signature.duration
        )
        # Whole note fits
        if timeleft >= duration:
            if isinstance(note, Rest):
                self.notes.append(note)
            else:
                self.notes.append(
                    Chord([note], joined=joined, articulations=articulations)
                )
            return

        # Fill up bar
        self.add_note_of_length(
            note, timeleft, joined=joined, articulations=articulations
        )

        duration -= timeleft

        # Fill up new bars
        while duration > self.time_signature.duration:
            duration -= self.time_signature.duration
            self.add_note_of_length(
                note,
                self.time_signature.duration,
                joined=True,
                articulations=articulations,
            )

        # Add remainder
        if duration != 0:
            self.add_note_of_length(
                note, duration, joined=True, articulations=articulations
            )

    def add_note_of_length(self, note, length, joined=False, articulations=None):
        notetype, dots = note_of_length(length)
        if isinstance(note, Rest):
            self.notes.append(Rest(notetype, dots))
        else:
            self.notes.append(
                Chord(
                    [Note(note.pitch, notetype, dots)],
                    joined=joined,
                    articulations=articulations,
                )
            )

    @property
    def duration(self):
        return sum(x.time for x in self.notes)

    def to_lilypond(self):
        notestext = []
        total = 0
        for note in self.notes:
            if not isinstance(note, Rest):
                lastpitch = note[0].pitch
                break
        else:
            # Irrelevant for rest-only
            lastpitch = None
        for i, chord in enumerate(self.notes):
            # Add pitches
            if isinstance(chord, Rest):
                pitches = "r"  # Rest
            else:
                lastchordpitch = lastpitch
                pitches = []
                for note in chord:
                    pitchtext = note.pitch.note.lower() + note.pitch.sharpnesstext()
                    effectivepitch = int(note.pitch)
                    # Change octave
                    while effectivepitch - int(lastchordpitch) >= 4:
                        pitchtext += "'"
                        effectivepitch -= 7
                    while int(lastchordpitch) - effectivepitch >= 4:
                        pitchtext += ","
                        effectivepitch += 7

                    lastchordpitch = note.pitch
                    pitches.append(pitchtext)
                lastpitch = chord[0].pitch

                pitches = " ".join(pitches)
                if len(chord) > 1:
                    pitches = f"<{pitches}>"
            # Add durations
            total += chord.time
            if chord.duration >= 3:
                durationstring = str(2 ** (chord.duration - 3))
            else:
                durationstring = "\\" + NOTENAMES[chord.duration]
            # Dots
            text = pitches + durationstring + "." * chord.dots

            # Articulations
            text += "".join(
                "-" + ARTICULATION_SHORT_NAMES[a]
                if a in ARTICULATION_SHORT_NAMES
                else "\\" + a.replace(" ", "")
                for a in chord.articulations
            )

            # Add slurs and ties
            with suppress(IndexError):
                if self.notes[i + 1].joined:
                    if chord.same_pitches(self.notes[i + 1]):
                        text += "~"
                    elif not chord.joined:
                        text += "("
            if chord.joined:
                try:
                    if chord.same_pitches(self.notes[i - 1]):
                        pass  # Tie
                    elif not self.notes[i + 1].joined:
                        text += ")"
                except IndexError:
                    text += ")"
            notestext.append(text)
            # Bar lines
            if abs(total % self.time_signature.duration) < 2**-30:
                notestext.append("|")
        return " ".join(notestext)


NORMALKEYS = {
    "C major/A minor": Key({}),
    "F major/D minor": Key.from_flats("B"),
    "B♭ major/G minor": Key.from_flats("BE"),
    "E♭ major/C minor": Key.from_flats("BEA"),
    "A♭ major/F minor": Key.from_flats("BEAD"),
    "D♭ major/B♭ minor": Key.from_flats("BEADG"),
    "G♭ major/E♭ minor": Key.from_flats("BEADGC"),
    "C♭ major/A♭ minor": Key.from_flats("BEADGCF"),
    "G major/E minor": Key.from_sharps("F"),
    "D major/B minor": Key.from_sharps("FC"),
    "A major/F♯ minor": Key.from_sharps("FCG"),
    "E major/C♯ minor": Key.from_sharps("FCGD"),
    "B major/G♯ minor": Key.from_sharps("FCGDA"),
    "F♯ major/D♯ minor": Key.from_sharps("FCGDAE"),
    "C♯ major/A♯ minor": Key.from_sharps("FCGDAEB"),
}

NORMALCLEFS = {
    "treble": Clef("G", 2),
    "bass": Clef("F", 6),
    "alto": Clef("C", 4),
    "tenor": Clef("C", 6),
}

COMMONTIMESIGNATUES = {
    f"{upper}/{lower}": TimeSignature(upper, lower)
    for upper, lower in (
        (4, 4),
        (3, 4),
        (2, 4),
        (2, 2),
        (3, 8),
        (6, 8),
        (9, 8),
        (12, 8),
        (5, 4),
        (6, 4),
    )
}
