import json
import math
from dataclasses import dataclass

import pygame

from . import music

pygame.init()


def imload(im):
    return pygame.image.load(f"graphics/{im}.png")


FONT_PATH = "fonts/Edwin/Edwin-Bold.otf"

BLACK = (0, 0, 0)
GREY = (119, 119, 119)

with open("fonts/Leland/leland_metadata.json") as f:
    FONT_METADATA = json.load(f)

METRICS = FONT_METADATA["engravingDefaults"]
TOPGAP = 7
STEM_LENGTH = 3.5
STEM_WIDTH = METRICS["stemThickness"]
NOTE_THICK = 0.2
STAFF_THICK = METRICS["staffLineThickness"]
BARLINE_THICK = METRICS["thinBarlineThickness"]
LEGER_THICK = METRICS["legerLineThickness"]
NOTESPACE = 2.5
LEGER_LENGTH = METRICS["legerLineExtension"]
STEM_UP_VERTICAL_OFFSET = FONT_METADATA["glyphsWithAnchors"]["noteheadBlack"]["stemUpSE"][1]
STEM_DOWN_VERTICAL_OFFSET = FONT_METADATA["glyphsWithAnchors"]["noteheadBlack"]["stemDownNW"][1]
DOT_GAP = 0.3
DOTSEP = 0.5
SLUR_THICKNESS = 0.1
SLUR_ANGLE = 0.15
SLUR_WIDTH = 0.8

KEYSIGSEP = 0.8

ARTICULATION_HEIGHT = 1

NOTE_HEADS = (0xE933, 0xE0A1, 0xE0A0, 0xE0A2, 0xE0A3) + (0x0E0A4,) * 99
RESTS = [0xE4E0 + i for i in range(14)]


def scale(*values):
    """Scale and round an lh value or iterable to pixel equivalents."""
    result = tuple(round(lh * i) for i in values)
    if len(result) == 1:
        return result[0]
    return result


ARTICULATIONS = {
    "ppppp": 0xE528,
    "pppp": 0xE529,
    "ppp": 0xE52A,
    "pp": 0xE52B,
    "p": 0xE520,
    "mp": 0xE52C,
    "mf": 0xE52D,
    "f": 0xE522,
    "ff": 0xE52F,
    "fff": 0xE530,
    "ffff": 0xE531,
    "fffff": 0xE532,
    "accent": 0xE4A0,
    "marcato": 0xE4AC,
    "staccato": 0xE4A2,
    "staccatissimo": 0xE4A6,
    "tenuto": 0xE4A4,
    "mordent": 0xE56D,
    "portato": 0xE4B2,
    "trill": 0xE566,
    "turn": 0xE567,
    "reverse turn": 0xE568,
    "fermata": 0xE4C0,
    "short fermata": 0xE4C4,
    "long fermata": 0xE4C6,
    "very long fermata": 0xE4C8,
}


def flag_code(number, direction):
    if direction == "up":
        return 0xE240 + (number - 1) * 2
    elif direction == "down":
        return 0xE241 + (number - 1) * 2


def draw_articulations(image, colour, articulations, note_x, topgap, music_font):
    if articulations is None:
        return
    no_articulation_font = pygame.font.Font(FONT_PATH, scale(0.25))
    for i, articulation in enumerate(articulations):
        draw_pos = scale(note_x, (topgap - (i + 1.5) * ARTICULATION_HEIGHT))
        if articulation in ARTICULATIONS:
            text = music_font.render(chr(ARTICULATIONS[articulation]), True, colour)
        else:
            text = no_articulation_font.render(articulation, 1, colour)
        image.blit(
            text,
            (draw_pos[0] - text.get_width() / 2, draw_pos[1] - text.get_height() / 2),
        )


@dataclass(repr=False)
class Piece(music.Piece):
    ydrawres: float

    def __post_init__(self):
        self.set_lh(self.ydrawres)
        self.note_areas_x = {}
        self.note_areas_y = {}
        self.background = None
        self.last_resolution = None

    def set_lh(self, image_height):
        self.lh = image_height / 20

    @property
    def lh(self):
        return self._lh

    @lh.setter
    def lh(self, value):
        self._lh = value
        self.ydrawres = value * 20
        global lh
        lh = self.lh
        self.music_font = pygame.font.Font("fonts/Leland/Leland.otf", round(4 * lh))
        self.accidental_images = [[None for _ in range(5)] for _ in range(2)]
        for i in range(5):
            code = (0xE261, 0xE262, 0xE263, 0xE264, 0xE260)[i]
            self.accidental_images[False][i] = self.music_font_symbol(code)
            self.accidental_images[True][i] = self.music_font_symbol(code, GREY)
        self.clef_image = self.music_font_symbol(
            {"G": 0xE050, "C": 0xE05C, "F": 0xE062}[self.clef.note]
        )
        self.rests = [self.music_font_symbol(i) for i in RESTS]
        self.note_heads = [{}, {}]
        for note_head in set(NOTE_HEADS):
            self.note_heads[False][note_head] = self.music_font_symbol(note_head)
            self.note_heads[True][note_head] = self.music_font_symbol(note_head, GREY)
        self.note_heads = [
            [self.note_heads[b][NOTE_HEADS[i]] for i in range(99)] for b in range(2)
        ]
        self.background = None

    def music_font_symbol(self, code, colour=(0, 0, 0)):
        return self.music_font.render(chr(code), True, colour)

    def resizegraphic(self, graphic, position, height):
        sf = self.lh * height / graphic.get_height()
        position = sf * position
        newsize = (round(sf * graphic.get_width()), round(height * self.lh))
        return (pygame.transform.smoothscale(graphic, newsize), position)

    def draw_clef(self, image):
        image.blit(self.clef_image, scale(1, TOPGAP - 4 - self.clef.position / 2))

    def draw_staff(self, image, drawres):
        # Draw staff
        for i in range(5):
            pygame.draw.line(
                image,
                BLACK,
                scale(0, TOPGAP + i),
                scale(drawres[0] / lh, i + TOPGAP),
                scale(STAFF_THICK),
            )

    def draw_key_signature(self, image):
        # Draw key signature
        pos = 4
        for ac in (-1, 1):
            for note in "FCGDAEB"[::ac]:
                position = int(music.Pitch(note, 5)) - self.clef.bottomline
                # Change octave to fit better
                while position >= 4 + 4:
                    position -= 7
                while position <= 4 - 4:
                    position += 7

                if self.key.notesharps[note] == ac:
                    accidental = ac
                    drawy = TOPGAP - 4 - position / 2
                    drawim = self.accidental_images[False][accidental]
                    image.blit(drawim, scale(pos, drawy))
                    pos += KEYSIGSEP
        return pos

    def draw_time_signature(self, image, pos):
        upperims, lowerims = (
            [self.music_font_symbol(0xE080 + int(c)) for c in str(x)]
            for x in self.time_signature
        )
        upper_width = sum(i.get_width() for i in upperims) / lh
        lower_width = sum(i.get_width() for i in lowerims) / lh
        width = max(upper_width, lower_width)
        p = (width - upper_width) / 2
        for i in upperims:
            image.blit(i, scale(pos + p, TOPGAP - 7))
            p += i.get_width() / lh
        p = (width - lower_width) / 2
        for i in lowerims:
            image.blit(i, scale(pos + p, TOPGAP - 5))
            p += i.get_width() / lh
        return width

    def draw_background(self, drawres):
        self.background = pygame.Surface(drawres)
        self.background.fill((255, 255, 255))

        self.draw_clef(self.background)

        # Draw staff
        self.draw_staff(self.background, drawres)

        # Draw key signature
        pos = self.draw_key_signature(self.background)

        pos += 0.2
        # Draw time signature
        width = self.draw_time_signature(self.background, pos)
        pos += width + 1
        self.start_pos = pos

    def draw_barline(self, image, drawpos, colour):
        pygame.draw.line(
            image,
            colour,
            scale(drawpos + NOTESPACE / 2, TOPGAP),
            scale(drawpos + NOTESPACE / 2, TOPGAP + 4),
            scale(BARLINE_THICK),
        )

    def draw(self, resolution, extra_note, aa=2):
        global lh
        lh = self.lh
        drawres = (resolution[0] * aa, resolution[1] * aa)
        if self.last_resolution != drawres:
            self.draw_background(drawres)
        self.last_resolution = drawres
        image = self.background.copy()
        pos = self.start_pos

        self.note_areas_x = {}
        self.note_areas_y = {}

        # Draw notes
        space = int((drawres[0] / lh - pos) / NOTESPACE) - 1
        if len(self.notes) > space:
            drawnnotes = self.notes[-space:]
        else:
            drawnnotes = self.notes[:]
        total = sum(note.time for note in self.notes if note not in drawnnotes)

        # Add in the extra note to self.notes
        edited = None
        if extra_note:
            if extra_note.pos == "new":
                drawnnotes.append(
                    music.Chord(
                        [extra_note.note],
                        joined=extra_note.joined,
                        articulations=extra_note.articulations,
                    )
                )
            else:
                if extra_note.pos in drawnnotes:
                    index = drawnnotes.index(extra_note.pos)
                    if drawnnotes[index].duration == extra_note.note.duration:
                        edited = drawnnotes[index]
                        drawnnotes[index] = music.Chord(
                            [*drawnnotes[index].notes, extra_note.note]
                        )
                        drawnnotes[index].articulations ^= extra_note.articulations

        # Find bar lines
        barafters = set()
        for chord in drawnnotes:
            total += chord.time
            if abs(total % self.time_signature.duration) < 2**-30:
                barafters.add(chord)

        # Find accidentals before the drawn music
        autosharps = self.key.notesharps.copy()
        for chord in self.notes:
            if chord in drawnnotes or chord is edited:
                break
            for note in chord:
                autosharps[note.pitch.note] = note.pitch.sharpness
            if chord in barafters:
                autosharps = self.key.notesharps.copy()

        # Start drawing the notes
        drawpos = pos
        for chord in drawnnotes:
            if not isinstance(chord, music.Rest):
                colour = self.draw_chord(image, chord, drawpos, extra_note, autosharps)
            else:
                colour = BLACK
                image.blit(
                    self.rests[chord.duration],
                    scale(
                        drawpos - self.rests[chord.duration].get_width() / lh / 2,
                        TOPGAP - 6,
                    ),
                )

            # Draw barlines
            if chord in barafters:
                self.draw_barline(image, drawpos, colour)
                autosharps = self.key.notesharps.copy()
            # Create click area
            if extra_note is None or extra_note.note in chord:
                target = "same"
            else:
                target = chord

            self.note_areas_x[
                scale((drawpos - NOTESPACE / 2) / aa, (drawpos + NOTESPACE / 2) / aa)
            ] = target

            drawpos += NOTESPACE

        for position in range(-10, 19):
            area = scale(
                (4 + TOPGAP - (position + 0.5) / 2) / aa,
                (4 + TOPGAP - (position - 0.5) / 2) / aa,
            )

            self.note_areas_y[area] = music.Pitch.from_int(
                self.clef.bottomline + position
            )
        if aa == 1:
            return image
        return pygame.transform.smoothscale(image, [round(i) for i in resolution])

    def draw_chord(self, image, chord, drawpos, extra_note, autosharps):
        note_positions = [int(note.pitch) - self.clef.bottomline for note in chord]
        meanpos = sum(note_positions) / len(note_positions)
        if meanpos < 4:
            stemup = True
            stem_base = -9999
            stem_end = 9999
        else:
            stemup = False
            stem_base = 9999
            stem_end = -9999
        current_autosharps = autosharps.copy()
        for note in chord:
            position = int(note.pitch) - self.clef.bottomline
            ypos = TOPGAP + 4 - position / 2
            colour = GREY if extra_note and note is extra_note.note else BLACK
            note_head = self.note_heads[colour == GREY][note.duration]

            # Draw head
            if note.duration == 0:
                # Draw maxima
                note_radius = 1.5
                rect = scale(
                    drawpos - note_radius, ypos - note_radius * 0.5, note_radius * 2, 1
                )
                pygame.draw.rect(image, colour, rect, scale(NOTE_THICK))
            else:
                note_radius = note_head.get_width() / 2 / lh
                image.blit(
                    note_head,
                    scale(
                        drawpos - note_radius, ypos - note_head.get_height() / 2 / lh
                    ),
                )

            # Draw dots
            if stemup:
                doty = scale(ypos - 0.3)
            else:
                doty = scale(ypos + 0.3)
            dot_image = self.music_font_symbol(0xE1E7, colour)
            for d in range(note.dots):
                dotpos = (
                    scale(
                        drawpos
                        + note_radius
                        + DOT_GAP
                        + d * DOTSEP
                        - dot_image.get_width() / 2 / lh
                    ),
                    doty - dot_image.get_height() / 2,
                )
                image.blit(dot_image, dotpos)

            # Draw leger lines
            legerx = (
                drawpos - note_radius * (1 + LEGER_LENGTH),
                drawpos + note_radius * (1 + LEGER_LENGTH),
            )
            if not -1 <= position <= 9:
                numberneeded = int(abs((position - 4) / 2) - 2)
                if position < -1:
                    line_ys = [i + 5 + TOPGAP for i in range(numberneeded)]
                elif position > 9:
                    line_ys = [-1 - i + TOPGAP for i in range(numberneeded)]
                for y in line_ys:
                    pygame.draw.line(
                        image,
                        colour,
                        scale(legerx[0], y),
                        scale(legerx[1], y),
                        scale(LEGER_THICK),
                    )

            # Determine the stem positions, for drawing later
            if stemup:
                stem_end = min(ypos - STEM_LENGTH, stem_end)
                stem_base = max(ypos, stem_base)
            else:
                stem_end = max(ypos + STEM_LENGTH, stem_end)
                stem_base = min(ypos, stem_base)

            # Draw accidentals
            if note.pitch.sharpness != current_autosharps[note.pitch.note]:
                autosharps[note.pitch.note] = note.pitch.sharpness
                accidental = note.pitch.sharpness
                drawy = TOPGAP - 4 - position / 2
                drawim = self.accidental_images[colour == GREY][accidental]
                image.blit(
                    drawim, scale(drawpos - 0.7 - drawim.get_width() / lh, drawy)
                )

            # Draw joins
            if chord.joined or (
                extra_note
                and extra_note.joined
                and any(extra_note.note is i for i in chord)
            ):
                if stemup:
                    angles = (math.pi * (1 + SLUR_ANGLE), math.pi * (2 - SLUR_ANGLE))
                else:
                    angles = (math.pi * SLUR_ANGLE, math.pi * (1 - SLUR_ANGLE))
                pygame.draw.arc(
                    image,
                    colour,
                    scale(
                        drawpos - (NOTESPACE + SLUR_WIDTH),
                        ypos - (note_radius + 0.5),
                        (NOTESPACE + SLUR_WIDTH * 2),
                        (note_radius * 2 + 1),
                    ),
                    angles[0],
                    angles[1],
                    scale(SLUR_THICKNESS),
                )

        if len(chord) > 1:
            colour = BLACK

        # Draw articulations
        draw_articulations(
            image,
            colour,
            chord.articulations,
            drawpos,
            TOPGAP - (max(max(note_positions) / 2, 3.5) - 3.5),
            self.music_font,
        )

        # Draw stem
        if chord.duration not in {2, 3}:
            if stemup:
                stemx = drawpos + (note_radius - STEM_WIDTH / 2)
                stem_base -= STEM_UP_VERTICAL_OFFSET
            else:
                stemx = drawpos - (note_radius - STEM_WIDTH / 2)
                stem_base -= STEM_DOWN_VERTICAL_OFFSET
            pygame.draw.line(
                image,
                colour,
                scale(stemx, stem_base),
                scale(stemx, stem_end),
                scale(STEM_WIDTH),
            )

        # Draw flags
        if chord.duration >= 6:
            flags = self.music_font_symbol(
                flag_code(chord.duration - 5, "up" if stemup else "down"), colour=colour
            )
            image.blit(
                flags,
                scale(stemx - STEM_WIDTH / 2, stem_end - flags.get_height() / 2 / lh),
            )
        return colour
